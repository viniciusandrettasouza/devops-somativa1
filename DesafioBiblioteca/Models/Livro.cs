﻿using System.ComponentModel.DataAnnotations;

namespace DesafioBiblioteca.Models
{
    public class Livro
    {
        public int Id { get; set; }
        [Display(Name = "Título")]
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public int Ano { get; set; }
        [Display(Name = "Gênero")]
        public Genero Genero { get; set; }

        public bool Emprestado { get; set; }
    }

    public enum Genero
    {
        Conto, 
        Fantasia, 
        [Display(Name = "Ficção Científica")] FiccaoCientifica, 
        Poesia,
        Romance  
    }
}
