﻿using DesafioBiblioteca.Models;
using Microsoft.AspNetCore.Mvc;

namespace DesafioBiblioteca.Controllers
{
    public class LivroController : Controller
    {
        public static List<Livro> _livros = new List<Livro>();
        private static int _id = 0;
        public IActionResult Index()
        {
            ViewBag.livros = _livros;
            return View();
        }

        [HttpPost]
        public IActionResult Cadastrar(Livro livro)
        {
            livro.Id = ++_id;
            livro.Emprestado = false;
            _livros.Add(livro);
            TempData["msg"] = "Livro cadastrado com sucesso!";
            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult Editar(int id)
        {
            Livro livro = _livros.Find(l => l.Id == id);
            return View(livro);
        }

        [HttpPost]
        public IActionResult Editar(Livro livro)
        {
            _livros[_livros.FindIndex(l => l.Id == livro.Id)] = livro;
            TempData["msg"] = "Livro editado com sucesso!";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Remover(int Id)
        {
            _livros.RemoveAll(l => l.Id == Id);
            TempData["msg"] = "Livro removido com sucesso!";
            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult Emprestar(int id)
        {
            _livros[_livros.FindIndex(l => l.Id == id)].Emprestado = true;
            TempData["msg"] = "Livro emprestado com sucesso!";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Devolver(int id)
        {
            _livros[_livros.FindIndex(l => l.Id == id)].Emprestado = false;
            TempData["msg"] = "Livro devolvido com sucesso!";
            return RedirectToAction("Emprestado");
        }

        public IActionResult Emprestado()
        {
            return View(_livros);
        }

    }
}
